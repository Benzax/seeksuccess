﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entites
{
    public class HttpResponseData
    {
        public string MessageType { get; set; }
        public string Data { get; set; }
        public string LogException { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public static class MessageType
    {
        public const string Error = "Error";
        public const string Info = "Info";
        public const string Waring = "Waring";
    }
}
