﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entites.Models
{
    public class ApplicationUser : IdentityUser, IEMEntity<string>
    {
        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is required")]
        public string LastName { get; set; }
        //[Required(ErrorMessage = "Phone is required")]
        //public string Phone { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        //[Required(ErrorMessage = "Dob is required")]
        //public DateTime Dob { get; set; }
        [Required(ErrorMessage = "Sexe is required")]
        public char Sexe { get; set; }
        //[Required(ErrorMessage = "Country is required")]
        //[ForeignKey(nameof(Country))]
        //public Guid CountryId { get; set; }
        //[Required(ErrorMessage = "LinkCv is required")]
        //public string LinkCv { get; set; }
        //[Required(ErrorMessage = "Status is required")]
        //public virtual int StatusId { get; set; }
        //[Required(ErrorMessage = "Secteur is required")]
        //public string Secteur { get; set; }
        //[Required(ErrorMessage = "Role is required")]
        //public string Role { get; set; }
    }
}
