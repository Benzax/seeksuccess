﻿using Contracts;
using Entites;
using Entites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class SecteurRepository : RepositoryBase<Secteur>, ISecteurRepository
    {
        public SecteurRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
