 using Contracts.Logger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using NLog;
using SeekSuccess_BackEnd.ConfigurationServices;
using System;
using System.IO;

namespace SeekSuccess_BackEnd
{
    public class Startup
    {
        public Startup(IConfiguration configuration )
        {
            Configuration = configuration;
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services )
        {

            services.AddControllers();
            services.ConfigureCors();

            //Configuration SqlContext
            services.ConfigureMySqlContext(Configuration);

            //Configuration Wrapper Dependency Injection
            services.ConfigureRepositoryWrapper();

            //Configuration Logger
            services.ConfigureLoggerService();

            //PipeLine for ExceptionFilter
            services.AddDatabaseDeveloperPageExceptionFilter();

            //Configuration Identity
            services.ConfigureIdentity();

            //Configuration Authentification and JWT
            services.ConfigureAuthentification(Configuration);


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SeekSuccess_BackEnd", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerManager logger)
        {
            app.ConfigureExceptionHandler(logger);


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SeekSuccess_BackEnd v1"));
            }
            else
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SeekSuccess_BackEnd v1"));

            }




            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


           

        }
    }
}
